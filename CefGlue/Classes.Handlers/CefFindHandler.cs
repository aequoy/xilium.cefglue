﻿namespace Xilium.CefGlue
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Runtime.InteropServices;
	using Xilium.CefGlue.Interop;

	/// <summary>
	/// Implement this interface to handle events related to dragging. The methods of
	/// this class will be called on the UI thread.
	/// </summary>
	public abstract unsafe partial class CefFindHandler
	{

		private void on_find_result(cef_find_handler_t* self, cef_browser_t* browser, int identifier, int count, cef_rect_t* selectionRect, int activeMatchOrdinal, int finalUpdate)
		{
			CheckSelf(self);

			var m_browser = CefBrowser.FromNative(browser);
			var m_selectionRect = new CefRectangle(selectionRect->x, selectionRect->y, selectionRect->width, selectionRect->height);

			OnFindResult(m_browser, identifier, count, m_selectionRect, activeMatchOrdinal, finalUpdate != 0 ? true : false);
		}

		protected abstract void OnFindResult(CefBrowser browser, int identifier, int count,
			CefRectangle selectionRect, int activeMatchOrdinal, bool finalUpdate);
	}
}
